

const { ccclass, property } = cc._decorator;

@ccclass
export default class AdaptScreenScene extends cc.Component {

    @property(cc.Node)
    btnShow: cc.Node = null;


    @property(cc.Prefab)
    pfPopView: cc.Prefab = null;



    onClickBtnShow() {
        let canvas = cc.find("Canvas");
        if (canvas) {
            let popView = cc.instantiate(this.pfPopView);
            popView.parent = canvas;
        }
    }


    // update (dt) {}
}
