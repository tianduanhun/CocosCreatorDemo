// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class PfSpriteToast extends cc.Component {
    @property(cc.Label)
    lbCount: cc.Label = null;

    @property(cc.Node)
    ndSprite: cc.Node = null;

    setData(data: any) {
        let itemId = data.itemId || 1;
        let count = data.count;

        this.lbCount.string = "+" + count;
    }

    onLoad() {
        cc.Tween.stopAllByTarget(this.node);
        let y = this.node.y;
        cc.tween(this.node).delay(2).to(1, { opacity: 0, y: y + 100 }).call(() => {
            this.node.destroy();
        }).start();
    }

    // update (dt) {}
}
